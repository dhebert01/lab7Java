package sandwich;

public class tofuSandwitch extends VegetarianSandwich{
 

    public tofuSandwitch() {
        this.addFilling("Tofu");
    }

    @Override
    public String getProtein() {
        return "Tofu";
    }
}