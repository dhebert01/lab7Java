package sandwich;
/*
 * Class for Ceaser sandwiches 
 * @author David Hebert
 * @version 18-10-2023
 */
public class CeaserSandwich extends VegetarianSandwich{
    private String filling;

    public CeaserSandwich(){
        this.filling = "“Caeser dressing";
    }

    public String getFilling(){
        /*
         * returns the toppings of the sandwich
         */
        return this.filling;
    }

    public String getProtein(){/*
        * returns the protein of the sandwich
        */
        return "Anchovies";
    }
    
    public boolean isVegetarian(){
        /*
        * returns false because will always have anchovies
        */
        return false;
    }
    
}
