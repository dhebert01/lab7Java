/**
 * This is a skeleton class for cylinders
 * @author Ivana Zhekova
 * @version 10/16/2023
 */

package sandwich;

public class BagelSandwich implements ISandwich {

    private String filling;

    /**
     * Constructot of BagelSandwich object with no filling.
     */

    public BagelSandwich() {
        this.filling = "";
    }

    /**
     * Adds specified topping to the filling of the bagel sandwich.
     * 
     * @param topping The topping to add to the sandwich's filling.
     */

    public void addFilling(String topping) {

        filling += topping;
    }

    /**
     * Retrieves the current filling of the bagel sandwich.
     * 
     * @return The filling of the bagel sandwich as a string.
     */

    public String getFilling() {

        return filling;
    }

    /**
     * Checks if the bagel sandwich is vegetarian.
     * Bagel sandwiches are not vegetarian by default.
     * 
     * @return true if the sandwich is vegetarian, false otherwise.
     */

    public boolean isVegetarian() {
        return false;
    }

}