package sandwich;

public abstract class VegetarianSandwich implements ISandwich{
    private String filling;

    public VegetarianSandwich(){
        this.filling = "";
    }

    public String getFilling(){
        /*
         * returns the toppings of the sandwich
         */
        return this.filling;
    }

    abstract String getProtein();

    public void addFilling(String topping){
        /*
         * takes a parameter topping that the user enters and it adds it to the sandwich
         */
        if(topping.contains("chicken") || topping.contains("beef") || topping.contains("fish") || topping.contains("meat") || topping.contains("pork")){
            throw new IllegalArgumentException("cannot add meat");
        }
        if(this.filling.equals("")){
            this.filling = topping;
        }
        else{
            this.filling = this.filling + ", " + topping;
        }
    }

    public boolean isVegetarian(){
        /*
         * returns if the sandwich is vegetarian or not
         */
        if (this.filling.contains("chicken") || this.filling.contains("beef") || this.filling.contains("fish") || this.filling.contains("meat") || this.filling.contains("pork")){
            return false;
        }
        else{
            return true;
        }
    }

    public boolean isVegan(){
        /*
         * returns if the sandwich is vegan or not
         */
        if(this.filling.contains("chicken") || this.filling.contains("beef") || this.filling.contains("fish") || this.filling.contains("meat") || this.filling.contains("pork")){
            return false;
        }
        else if(this.filling.contains("egg") || this.filling.contains("cheese")){
            return false;
        }
        else{
            return true;
        }
    }
    
}
