/**
 * Unit tests for the Vegetarian Sandwitch
 * @author Ivana Zhekova
 * @version 10/16/2023
 */

package sandwich;

import static org.junit.Assert.*;

import org.junit.Test;

public class testVegetarianSandwitch {

    @Test
    public void testGetFilling() {
        VegetarianSandwich sandwich = new tofuSandwitch();
        assertEquals("Tofu", sandwich.getFilling());
    }

    @Test
    public void testAddFillingVegetarian() {
        VegetarianSandwich sandwich = new tofuSandwitch();
        sandwich.addFilling("lettuce");
        assertEquals("Tofu, lettuce", sandwich.getFilling());
    }

    @Test
    public void testAddFillingNonVegetarian() {
        VegetarianSandwich sandwich = new tofuSandwitch();
        
        try {
            sandwich.addFilling("chicken");
            fail("Expected an IllegalArgumentException to be thrown.");
        } catch (IllegalArgumentException e) {
          //expected 
        }
    }

    @Test
    public void testIsVegetarian() {
        VegetarianSandwich sandwich = new tofuSandwitch();
        assertTrue(sandwich.isVegetarian());
    }

    @Test 
    public void testNotIsVegan(){
            VegetarianSandwich sandwich = new tofuSandwitch();
            sandwich.addFilling("egg");
            assertEquals(false, sandwich.isVegan());
    }

    @Test
       public void testIsVegan(){
            VegetarianSandwich sandwich = new tofuSandwitch();
            sandwich.addFilling("tomato");
            assertEquals(true, sandwich.isVegan());
    }

    
}
