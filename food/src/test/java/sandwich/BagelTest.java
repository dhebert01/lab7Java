package sandwich;
import static org.junit.Assert.*;

import org.junit.Test; 
/**
 * @Author David Hebert
 * @Version 10/16/2023
 * Tests the bagelSandwich class
 */
public class BagelTest {

    @Test
    public void testAddFilling(){
        //test add filling
        BagelSandwich sand1 = new BagelSandwich();
        sand1.addFilling("eggs");

        assertEquals("eggs",sand1.getFilling());
    }

    @Test
    public void testGetFilling(){
        //test both contructor and getFilling
        BagelSandwich sand1 = new BagelSandwich();
        assertEquals("", sand1.getFilling());
    }

    @Test
    public void testIsVegetarian(){
        //test isVegetarian method should retunr false
        BagelSandwich sand1 = new BagelSandwich();
        sand1.addFilling("chicken");
        assertEquals(false, sand1.isVegetarian());
    }
}
